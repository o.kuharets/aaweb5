<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
// Будем сохранять туда логин после успешной авторизации.
if (!empty($_SESSION['login'])) {
  session_destroy();
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  header('Location: ./');
}

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	$messages=array();
	$errors=array();
	$errors['logipar']=!empty($_COOKIE['logipar_error']);
	if ($errors['logipar'])
	{
		setcookie('logipar_error', '', 100000);
		$messages[]='<div class="error"> Не проходит логин!</div>';
	}
?>

<style>
.error {
  border: 2px solid red;
}
    </style>
    <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}?>
<head>
  <link rel="stylesheet" href=style.css>
  <title>Авторизация</title>
</head>
<header>

</header>

<body>


  <div class="content">
    </br>
    <div id="vhod">
      <a href="index.php">Назад</a>
    </div>
    <h2 id="an">Авторизация</h2>
    <form action="" method="post">
      <label>
        Логин:</br>
        <input name="login" <?php if ($errors['logipar']) {print 'class="error"' ;} ?> />
      </label>
      </br>
      <label>
        Пароль:</br>
        <input name="pass" type="password" <?php if ($errors['logipar']) {print 'class="error"' ;} ?>/>
      </label>
      </br>
      <input type="submit" value="Войти" />
    </form>
  </div>
</body>
<footer>
  <p>(с)Кухарец Ольга 2021</p>
</footer>
<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
	$errors=false;
	
  $user = 'u23969';
  $pass = '1986532';
  $db = new PDO('mysql:host=localhost;dbname=u23969', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

  // TODO: Проверть есть ли такой логин и пароль в базе данных.
  // Выдать сообщение об ошибках.
$log=$db->quote($_POST['login']);
$par=md5($_POST['pass']);

$rez_zap=$db->query("SELECT login FROM logipar WHERE login=$log"); 
foreach ($rez_zap as $elem)
	$login2=$elem['login'];
$rez_zap=$db->query("SELECT parol FROM logipar WHERE login=$log"); 
foreach ($rez_zap as $elem)
	$parol2=$elem['parol'];



if (!empty($login2) && !empty($parol2) && $parol2==$par){
  // Если все ок, то авторизуем пользователя.
  $_SESSION['login'] = $_POST['login'];
  $pp = $db->quote($parol2);
  $rez_zap=$db->query("SELECT id FROM logipar WHERE login=$log AND parol=$pp");
  foreach ($rez_zap as $elem) 
  	$id=(int)$elem['id']; 	
  // Записываем ID пользователя.
  $_SESSION['uid'] = $id;
}
else { 
	setcookie('logipar_error', '1', time() + 24 * 60 * 60);//Сохраняем ошибочную куку
    $errors = TRUE;
}
 if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: login.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('logipar_error', '', 100000);
    }

  // Делаем перенаправление.
  header('Location: ./');
}
//5dedd0    5e0c1d 
//1c61ce    da3d7b 
