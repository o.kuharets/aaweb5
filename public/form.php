<!DOCTYPE html>
<html lang="ru">
<head >
	    <title>Задание 5</title>
	    <link rel="stylesheet" href=style.css >
	    <meta charset="utf-8">
	    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
    </style>
    <?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}?>
</head>

<body>


  <header>
    </br></br>
    <div id="auth">
      <?php if (empty($_SESSION['login'])) { ?>
      <a href="login.php">Вход</a>
      <?php } else { ?> <a href="login.php">Выход</a>
      <?php } ?>
    </div>
  </header>

  <div id="form" class="content">
    <h2 id="an">Форма</h2>

    <form action="" method="POST">

      <label>
        Имя:<br />
        <input name="field-name-1" <?php if ($errors['field-name-1']) {print 'class="error"' ;} ?> value="
        <?php print $values['field-name-1'];?>" />
      </label><br />
      <label>
        email:<br />
        <input name="field-email" type="email" <?php if ($errors['field-email']) {print 'class="error"' ;} ?> value="
        <?php print $values['field-email']; ?>" />
      </label><br />

      <label>
        Дата Рождения:<br />
        <input name="field-date" type="date" <?php if ($errors['field-date']) {print 'class="error"' ;} ?> value="
        <?php print $values['field-date']; ?>"/>
      </label><br />


      Пол:<br />
      <label><input type="radio" name="radio-group-1" value="1" <?php if ($errors['radio-group-1'])
          {print 'class="error"' ;} ?>
        <?php if ($values['radio-group-1']==1){print 'checked';}?>/>
        М
      </label>
      <label><input type="radio" name="radio-group-1" value="2" <?php if ($errors['radio-group-1'])
          {print 'class="error"' ;} ?>
        <?php if ($values['radio-group-1']==2){print 'checked';}?>/>
        Ж
      </label><br />


      Количество конечностей:<br />
      <label><input type="radio" name="radio-group-2" value="1" <?php if ($errors['radio-group-2'])
          {print 'class="error"' ;} ?>
        <?php if ($values['radio-group-2']==1){print 'checked';}?> />
        1
      </label>
      <label><input type="radio" name="radio-group-2" value="2" <?php if ($errors['radio-group-2'])
          {print 'class="error"' ;} ?>
        <?php if ($values['radio-group-2']==2){print 'checked';}?>/>
        2
      </label><br />
      <label><input type="radio" name="radio-group-2" value="3" <?php if ($errors['radio-group-2'])
          {print 'class="error"' ;} ?>
        <?php if ($values['radio-group-2']==3){print 'checked';}?>/>
        3
      </label>
      <label><input type="radio" name="radio-group-2" value="4" <?php if ($errors['radio-group-2'])
          {print 'class="error"' ;} ?>
        <?php if ($values['radio-group-2']==4){print 'checked';}?>/>
        4
      </label><br />
      <label><input type="radio" name="radio-group-2" value="5" <?php if ($errors['radio-group-2'])
          {print 'class="error"' ;} ?>
        <?php if ($values['radio-group-2']==5){print 'checked';}?>/>
        Больше
      </label><br />

      <label>
        Суперсила:
        <br />
        <select name="field-name-4[]" multiple="multiple">
          <option value="1" <?php $mas=str_split($values['field-name-4']); foreach($mas as $lol) if ($lol==1)
            print 'selected' ; ?>
            <?php if ($errors['field-name-4']) {print 'class="error"';} ?> >телекинез
          </option>
          <option value="2" <?php $mas=str_split($values['field-name-4']); foreach($mas as $lol) if ($lol==2)
            print 'selected' ; ?>
            <?php if ($errors['field-name-4']) {print 'class="error"';} ?>>чтение мыслей
          </option>
          <option value="3" <?php $mas=str_split($values['field-name-4']); foreach($mas as $lol) if ($lol==3)
            print 'selected' ; ?>
            <?php if ($errors['field-name-4']) {print 'class="error"';} ?>>магия
          </option>
          <option value="4" <?php $mas=str_split($values['field-name-4']); foreach($mas as $lol) if ($lol==4)
            print 'selected' ; ?>
            <?php if ($errors['field-name-4']) {print 'class="error"';} ?>>суперсила
          </option>
        </select>
      </label><br />

      <label>
        Биография:<br />
        <textarea name="field-name-2" <?php if ($errors['field-name-2']) {print 'class="error"' ;}
          ?>><?php print $values['field-name-2']; ?></textarea>
      </label><br />

      Со всем выше написанным ознакомлен:<br />
      <label><input type="checkbox" checked="checked" name="check-1" <?php if ($errors['check-1'])
          {print 'class="error"' ;} ?> />
        ДА</label><br />


      <input type="submit" value="Отправить" />
    </form>
  </div>
  <footer>
    <p>(с)Кухарец Ольга 2021</p>
  </footer>
</body>

</html>
